package com.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class EmailValidatorTest {

    private EmailValidator valid = new EmailValidator();

    @Test
    public void shouldValidEmail() {
        Assertions.assertTrue(valid.isValid("asasas@gmail.com"));
    }

    @Test
    public void shouldInvalidMissing() {
        Assertions.assertFalse(valid.isValid("anthor@"));
    }

    @Test
    public void shouldInvalidTooShortPreAt() {
        Assertions.assertFalse(valid.isValid("a@dzdzd.fr"));
    }
}