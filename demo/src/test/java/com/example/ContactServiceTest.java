package com.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class ContactServiceTest {

    private ContactService service = new ContactService();

    @Test
    public void shoulInsertdValidEmail() {
        service.addContact("author@mail.com");
    }

    @Test
    public void shouldRefuseInValidEmail() {
        Assertions.assertThrows(RuntimeException.class, () -> service.addContact("author@"));
    }

    @Test
    public void shouldRaiseExceptionOnDuplicateEmail() {
        service.addContact("author@mail.com");
        Assertions.assertThrows(RuntimeException.class, () -> service.addContact("author@mail.com"));
    }

}