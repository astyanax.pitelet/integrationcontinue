package com.example;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.stubbing.Answer;

@MockitoSettings
public class ContactServiceMockTest {
    
    @InjectMocks
    private ContactService service = new ContactService();

    @Mock(lenient = true)
    private ContactDao dao;

    
    @Test 
    void shloudInteruptFetch() {
        Mockito.when(dao.fetchAll()).thenAnswer(x -> {
            Thread.sleep(4000);
            return new ArrayList<>();
        });
        Assertions.assertThrows(InterruptedException.class, () -> dao.fetchAll());
    }

    @Test 
    void shloudTest() {
        Mockito.doThrow(new RuntimeException("error")).when(dao).add("test@gmail.com");
        service.addContact("test@gmail.com");
    }

}
