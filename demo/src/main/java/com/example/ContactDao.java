package com.example;

import java.util.ArrayList;

public class ContactDao {
    
    private ArrayList<String> emails = new ArrayList<>();

    public void add(String mail) {
        if(this.emails.contains(mail)) {
            throw new RuntimeException("error");
        }
        this.emails.add(mail);
    }

    public void delete(String mail) {
        if(!this.emails.contains(mail)) {
            throw new RuntimeException("est inconnue");
        }
        this.emails.remove(mail);
    }

    public ArrayList<String> fetchAll() {
        return this.emails;
    }

    

}
