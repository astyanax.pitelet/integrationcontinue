package com.example;

import java.util.regex.Pattern;

public class EmailValidator {

    String regex = "\\w{2,64}@\\w{3,20}\\.\\w{2,3}";

    public boolean isValid(String str) {
        if(str == null) {
            return false;
        }
        return Pattern.compile(regex).matcher(str).matches();
    }

}