package com.example;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ContactService {
    
    private EmailValidator valid = new EmailValidator();
    private ContactDao contactDao = new ContactDao();

    public void addContact(String contact) {
        if(this.valid.isValid(contact)) {
            this.contactDao.add(contact);
        } else {
            throw new RuntimeException("email is invalid");
        }
    }

    public void deleteContact(String contact) {
        try {
            contactDao.delete(contact);
        } catch(RuntimeException e) {

        }
    }

    public ArrayList<String> getContacts() throws InterruptedException, ExecutionException, TimeoutException {
        return Executors.newSingleThreadExecutor().submit(() -> contactDao.fetchAll()).get(3, TimeUnit.SECONDS);
    }

}
